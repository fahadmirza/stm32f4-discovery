/**
  ******************************************************************************
  * @file    main.h 
  * @author  Fahad Mirza
  * @version V1.0
  * @date    28-June-2016
  * @brief   Header for main.c
  ******************************************************************************
  */
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4_discovery.h"
#include "stdbool.h"
#include "lsm9ds0.h"

  
/* Exported macro ------------------------------------------------------------*/
#define COUNTOF(__BUFFER__)   (sizeof(__BUFFER__) / sizeof(*(__BUFFER__)))



/* Private variables ---------------------------------------------------------*/
// UART handler declaration
UART_HandleTypeDef UartHandle;



/* Function Definitions ------------------------------------------------------*/
HAL_StatusTypeDef UART_Init(void);
void UART_Transmit(const uint8_t *str);


void Sensor_Data_Display(const lsm9ds0Vector_t *accelData, const lsm9ds0Vector_t *magData, const lsm9ds0Vector_t *gyroData);


void     reverse(uint8_t *str, uint16_t len);
uint16_t intToStr(int16_t num, uint8_t str[]);
int16_t  abs(int16_t num);



/* Function Declarations -----------------------------------------------------*/
/**
  * @brief  UART Initialization
  * @param  None
  * @retval HAL Status
  */
HAL_StatusTypeDef UART_Init(void)
{
  // UART2 configuration
  UartHandle.Instance          = USART2;
  UartHandle.Init.BaudRate     = 9600;
  UartHandle.Init.WordLength   = UART_WORDLENGTH_8B;
  UartHandle.Init.StopBits     = UART_STOPBITS_1;
  UartHandle.Init.Parity       = UART_PARITY_NONE;
  UartHandle.Init.HwFlowCtl    = UART_HWCONTROL_NONE;
  UartHandle.Init.Mode         = UART_MODE_TX_RX;
  UartHandle.Init.OverSampling = UART_OVERSAMPLING_16;

  // This function calls HAL_I2C_MspInit(). Modify it in stm32f4xx_hal_msp.c
  return HAL_UART_Init(&UartHandle);
}



/**
  * @brief  UART Bytes/String Transmission
  * @param  str: Buffer Handle pointer
  * @retval None
  */
void UART_Transmit(const uint8_t *str)
{
  int str_len;
  for(str_len=0; str[str_len]!=0; str_len++);
  
  HAL_UART_Transmit(&UartHandle, (uint8_t*)str, str_len, TIMEOUT);
}



/**
  * @brief  Raw LSM9DS0 sensor data display
  * @param  accelData,magData,gyroData: lsm9ds0 Data Handle pointer
  * @retval None
  */
void Sensor_Data_Display(const lsm9ds0Vector_t *accelData, const lsm9ds0Vector_t *magData, const lsm9ds0Vector_t *gyroData)
{
  uint8_t str[10];
  
  UART_Transmit("\nACC:\tX="); intToStr(accelData->x, str); UART_Transmit(str);
  UART_Transmit("\tY="); intToStr(accelData->y, str); UART_Transmit(str);                
  UART_Transmit("\tZ="); intToStr(accelData->z, str); UART_Transmit(str);
  
  UART_Transmit("\nMAG:\tX="); intToStr(magData->x, str); UART_Transmit(str);
  UART_Transmit("\tY="); intToStr(magData->y, str); UART_Transmit(str);                
  UART_Transmit("\tZ="); intToStr(magData->z, str); UART_Transmit(str);
  
  UART_Transmit("\nGYRO:\tX="); intToStr(gyroData->x, str); UART_Transmit(str);
  UART_Transmit("\tY="); intToStr(gyroData->y, str); UART_Transmit(str);                
  UART_Transmit("\tZ="); intToStr(gyroData->z, str); UART_Transmit(str);
  UART_Transmit("\n");
}


/**
  * @brief  int (signed) to string converter (-32768 to 32768)
  * @param  num: integer number    
  *         str: buffer pointer
  * @retval Length of buffer
  */
uint16_t intToStr(int16_t num, uint8_t str[])
{
    uint16_t i = 0;
    bool negativeNum = false;
    if(num<0)
    {
      str[i++] = '-';
      negativeNum = true;
      num = -num;
    }
    while (num)
    {
        str[i++] = (num%10) + '0';
        num = num/10;
    }
    
    if(negativeNum)
      reverse(str+1, i-1);
    else
      reverse(str, i);
    str[i] = '\0';
    return i;
}

/**
  * @brief  Reverses a string 'str' of length 'len'
  * @param  str: buffer pointer   
  *         len: buffer length
  * @retval None
  */
void reverse(uint8_t *str, uint16_t len)
{
    uint16_t i=0, j=len-1; 
    uint8_t temp;
    while (i<j)
    {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++; j--;
    }
}


/**
  * @brief  Provides absolute value of a signed integer
  * @param  num: Signed integer
  * @retval Absolute value
  */
int16_t abs(int16_t num)
{
  return ((num<0) ? -num : num);
}
   
 
#endif /* __MAIN_H */