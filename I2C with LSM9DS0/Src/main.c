/**
  ******************************************************************************
  * @file       main.c 
  * @author     Fahad Mirza
  * @version    V1.0
  * @date       28-June-2016
  * @modified   30-June-2016
  * @brief      Main program body
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"


/* Private define ------------------------------------------------------------*/
#define FREEFALL_MODE
//#define STREAM_MODE

#define FREEFALL_THRESHOLD 7000



/* Private variables ---------------------------------------------------------*/
// I2C handler declaration
I2C_HandleTypeDef I2cHandle;


// Constant Strings
const uint8_t Welcome[] = "Welcome!\r\n";
const uint8_t Error[] = "Error!!!";
const uint8_t Ready[] = "Ready.\r\n";
const uint8_t DevNotFound[] = "Device is not available.\r\n";
const uint8_t Freefall[] = "Freefall!!!!\r\n";



/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);
static void Error_Handler(void);



/* Private functions ---------------------------------------------------------*/
/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{

  /* STM32F4xx HAL library initialization:
       - Configure the Flash prefetch, Flash preread and Buffer caches
       - Systick timer is configured with Time base as 1ms 
         since PPP_TIMEOUT_VALUEs are defined and handled in milliseconds basis.
       - Low Level Initialization
  */
  HAL_Init();

  // Configure the system clock to 168 MHz 
  SystemClock_Config();

  // Configure LEDs 
  BSP_LED_Init(LED3);
  BSP_LED_Init(LED6);
  
  if(UART_Init() != HAL_OK)
  {
    Error_Handler();
  }
  
  
  // Initialize LSM9DS0
  if(lsm9dso_init(&I2cHandle) != HAL_OK)
  {
    UART_Transmit(DevNotFound);
    Error_Handler();
  }

  
  // Configure KEY Button
  BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_GPIO);
  
  // Wait for USER Button press
  while (BSP_PB_GetState(BUTTON_KEY) == RESET)
  {
    // Toggle LED3 until user press the button
    BSP_LED_Toggle(LED3);
    HAL_Delay(50);		
  }
  // Wait for USER Button release
  while (BSP_PB_GetState(BUTTON_KEY) == SET);
  BSP_LED_Off(LED3);
  
  
  // Transmit the string (TxBuffer)
  UART_Transmit(Welcome);
    
  // Accelerometer, Magnetometer and Gyro data storage
  lsm9ds0Vector_t accelData;
  lsm9ds0Vector_t magData;  
  lsm9ds0Vector_t gyroData;

  while (1)
  {
    // Read Sensor Data
    lsm9ds0_Accel_Data_Read(&I2cHandle, &accelData);
    lsm9ds0_Mag_Data_Read(&I2cHandle, &magData);
    lsm9ds0_Gyro_Data_Read(&I2cHandle, &gyroData);
    
#ifdef STREAM_MODE     
    Sensor_Data_Display(&accelData, &magData, &gyroData);
#elif defined FREEFALL_MODE
    // Free Fall detection
    if((abs(accelData.x) + abs(accelData.y) + abs(accelData.z))<FREEFALL_THRESHOLD)
    {
      UART_Transmit(Freefall);
    }
#endif
    
    HAL_Delay(20);
  }
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 168000000
  *            HCLK(Hz)                       = 168000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 4
  *            APB2 Prescaler                 = 2
  *            HSE Frequency(Hz)              = 8000000
  *            PLL_M                          = 8
  *            PLL_N                          = 336
  *            PLL_P                          = 2
  *            PLL_Q                          = 7
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 5
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;
  
  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();
  
  /* The voltage scaling allows optimizing the power consumption when the device is 
     clocked below the maximum system frequency, to update the voltage scaling value 
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }
  
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;  
  if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  /* STM32F405x/407x/415x/417x Revision Z devices: prefetch is supported  */
  if (HAL_GetREVID() == 0x1001)
  {
    /* Enable the Flash prefetch */
    __HAL_FLASH_PREFETCH_BUFFER_ENABLE();
  }
}
/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
  BSP_LED_On(LED6);
  while(1)
  {
  }
}


