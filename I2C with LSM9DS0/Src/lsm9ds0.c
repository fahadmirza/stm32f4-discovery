/**
  ******************************************************************************
  * @file      lsm9ds0.c 
  * @author    Fahad Mirza
  * @version   V1.0
  * @date      29-June-2016
  * @modified  30-June-2016
  * @brief     LSM9DS0 Driver
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "lsm9ds0.h"



/**
  * @brief  Initialize I2C1 and configure LSM9DS0.
  * @note   This function calls HAL_I2C_MspInit(). Modify it in stm32f4xx_hal_msp.c
  * @param  hi2c: I2C handle pointer
  * @retval HAL Status
  */
HAL_StatusTypeDef lsm9dso_init(I2C_HandleTypeDef *hi2c)
{
  // Configure the I2C peripheral
  hi2c->Instance                = I2C1;
  hi2c->Init.AddressingMode     = I2C_ADDRESSINGMODE_7BIT;
  hi2c->Init.ClockSpeed         = 100000;
  hi2c->Init.DualAddressMode    = I2C_DUALADDRESS_DISABLE;
  hi2c->Init.DutyCycle          = I2C_DUTYCYCLE_2;
  hi2c->Init.GeneralCallMode    = I2C_GENERALCALL_DISABLE;
  hi2c->Init.NoStretchMode      = I2C_NOSTRETCH_DISABLE;
  hi2c->Init.OwnAddress1        = 0x00; // Not necessary
  
  if(HAL_I2C_Init(hi2c) != HAL_OK)      // Initialize I2C.                
  {
    return HAL_ERROR;    
  }
  if(HAL_I2C_IsDeviceReady(hi2c, LSM9DS0_ADDRESS_ACCELMAG, 10, TIMEOUT) != HAL_OK)
  {
    return HAL_ERROR;
  }
  
  lsm9ds0_configure(hi2c);              // Configure the sensor parameters
  
  return HAL_OK;
}

  
/**
  * @brief  Configure LSM9DS0.
  * @param  hi2c: I2C handle pointer
  * @retval none
  */  
void lsm9ds0_configure(I2C_HandleTypeDef *hi2c)
{
  // 95Hz rate, Enable Gyro, Enable axes
  lsm9ds0_Gyro_Mem_Write(hi2c, LSM9DS0_REGISTER_CTRL_REG1_G, 0x0F);
  // 100Hz rate, cont. update, enable axes
  lsm9ds0_AccelMag_Mem_Write(hi2c, LSM9DS0_REGISTER_CTRL_REG1_XM, 0x67);
  // Temp sen disabled, Mag hi res, 50Hz rate, no interrupt
  lsm9ds0_AccelMag_Mem_Write(hi2c, LSM9DS0_REGISTER_CTRL_REG5_XM, 0x70);
  // No filter, cont. update
  lsm9ds0_AccelMag_Mem_Write(hi2c, LSM9DS0_REGISTER_CTRL_REG7_XM, 0x00);
  
  // Acceleration 2G
  uint8_t reg = lsm9ds0_AccelMag_Mem_Read(hi2c,LSM9DS0_REGISTER_CTRL_REG2_XM);
  reg &= ~(0x38);//0b00111000
  reg |= LSM9DS0_ACCELRANGE_2G;
  lsm9ds0_AccelMag_Mem_Write(hi2c, LSM9DS0_REGISTER_CTRL_REG2_XM, reg);
  
  // Magnet 2 Gauss
  reg = lsm9ds0_AccelMag_Mem_Read(hi2c,LSM9DS0_REGISTER_CTRL_REG6_XM);
  reg &= ~(0x60); //0b01100000
  reg |= LSM9DS0_MAGGAIN_2GAUSS;
  lsm9ds0_AccelMag_Mem_Write(hi2c, LSM9DS0_REGISTER_CTRL_REG6_XM, reg);
  
  // Gyro 245 DPS
  reg = lsm9ds0_Gyro_Mem_Read(hi2c,LSM9DS0_REGISTER_CTRL_REG4_G);
  reg &= ~(0x30); //0b00110000
  reg |= LSM9DS0_GYROSCALE_245DPS;
  lsm9ds0_Gyro_Mem_Write(hi2c, LSM9DS0_REGISTER_CTRL_REG4_G, reg);
}


/**
  * @brief  Read single register from Accelerometer or Magnetometer.
  * @param  hi2c: I2C handle pointer, 
  *         MemAddress: Register Address
  * @retval Register value
  */
uint8_t lsm9ds0_AccelMag_Mem_Read(I2C_HandleTypeDef *hi2c, uint16_t MemAddress)
{
  uint8_t temp_data_holder;
  HAL_I2C_Mem_Read(hi2c, LSM9DS0_ADDRESS_ACCELMAG, MemAddress, I2C_MEMADD_SIZE_8BIT,  &temp_data_holder, 1, TIMEOUT);
  
  return temp_data_holder;
}

/**
  * @brief  Read multiple registers from Accelerometer or Magnetometer.
  * @param  hi2c: I2C handle pointer, 
  *         MemAddress: Register Address
  *         buffer: Data holder
  *         Size: Number of bytes to read
  * @retval None
  */
void lsm9ds0_AccelMag_Mem_Readbytes(I2C_HandleTypeDef *hi2c, uint16_t MemAddress, uint8_t *buffer, uint16_t Size)
{
  HAL_I2C_Mem_Read(hi2c, LSM9DS0_ADDRESS_ACCELMAG, MemAddress, I2C_MEMADD_SIZE_8BIT, buffer, Size, TIMEOUT);
}

/**
  * @brief  Write to single register in Accelerometer or Magnetometer.
  * @param  hi2c: I2C handle pointer, 
  *         MemAddress: Register Address
  *         data: byte to write
  * @retval None
  */
void lsm9ds0_AccelMag_Mem_Write(I2C_HandleTypeDef *hi2c, uint16_t MemAddress, uint8_t data)
{
  HAL_I2C_Mem_Write(hi2c, LSM9DS0_ADDRESS_ACCELMAG, MemAddress, I2C_MEMADD_SIZE_8BIT,  &data, 1, TIMEOUT);
}


/**
  * @brief  Read single register from Gyroscope.
  * @param  hi2c: I2C handle pointer, 
  *         MemAddress: Register Address
  * @retval Register value
  */
uint8_t lsm9ds0_Gyro_Mem_Read(I2C_HandleTypeDef *hi2c, uint16_t MemAddress)
{
  uint8_t temp_data_holder;
  HAL_I2C_Mem_Read(hi2c, LSM9DS0_ADDRESS_GYRO, MemAddress, I2C_MEMADD_SIZE_8BIT,  &temp_data_holder, 1, TIMEOUT);
  
  return temp_data_holder;
}

/**
  * @brief  Read multiple registers from Gyroscope.
  * @param  hi2c: I2C handle pointer, 
  *         MemAddress: Register Address
  *         buffer: Data holder
  *         Size: Number of bytes to read
  * @retval None
  */
void lsm9ds0_Gyro_Mem_Readbytes(I2C_HandleTypeDef *hi2c, uint16_t MemAddress, uint8_t *buffer, uint16_t Size)
{
  HAL_I2C_Mem_Read(hi2c, LSM9DS0_ADDRESS_GYRO, MemAddress, I2C_MEMADD_SIZE_8BIT, buffer, Size, TIMEOUT);
}

/**
  * @brief  Write to single register in Gyroscope.
  * @param  hi2c: I2C handle pointer, 
  *         MemAddress: Register Address
  *         data: byte to write
  * @retval None
  */
void lsm9ds0_Gyro_Mem_Write(I2C_HandleTypeDef *hi2c, uint16_t MemAddress, uint8_t data)
{
  HAL_I2C_Mem_Write(hi2c, LSM9DS0_ADDRESS_GYRO, MemAddress, I2C_MEMADD_SIZE_8BIT,  &data, 1, TIMEOUT);
}



/**
  * @brief  Read Accel data.
  * @param  hi2c: I2C handle pointer, 
  *         accelData: lsm9ds0 Data Handle pointer
  * @retval None
  */
void lsm9ds0_Accel_Data_Read(I2C_HandleTypeDef *hi2c, lsm9ds0Vector_t *accelData)
{
  uint8_t buffer[6];
  lsm9ds0_AccelMag_Mem_Readbytes(hi2c, 0x80 | LSM9DS0_REGISTER_OUT_X_L_A, buffer, 6);
  
  // Shift values to create properly formed integer (low byte first)
  accelData->x = (buffer[1] << 8) | buffer[0];
  accelData->y = (buffer[3] << 8) | buffer[2];
  accelData->z = (buffer[5] << 8) | buffer[4];
}


/**
  * @brief  Read Mag Data.
  * @param  hi2c: I2C handle pointer, 
  *         magData: lsm9ds0 Data Handle pointer
  * @retval None
  */
void lsm9ds0_Mag_Data_Read(I2C_HandleTypeDef *hi2c, lsm9ds0Vector_t *magData)
{
  uint8_t buffer[6];
  lsm9ds0_AccelMag_Mem_Readbytes(hi2c, 0x80 | LSM9DS0_REGISTER_OUT_X_L_M, buffer, 6);
  
  // Shift values to create properly formed integer (low byte first)
  magData->x = (buffer[1] << 8) | buffer[0];
  magData->y = (buffer[3] << 8) | buffer[2];
  magData->z = (buffer[5] << 8) | buffer[4];
}


/**
  * @brief  Read gyroscope Data.
  * @param  hi2c: I2C handle pointer, 
  *         gyroData: lsm9ds0 Data Handle pointer
  * @retval None
  */
void lsm9ds0_Gyro_Data_Read(I2C_HandleTypeDef *hi2c, lsm9ds0Vector_t *gyroData)
{
  uint8_t buffer[6];
  lsm9ds0_Gyro_Mem_Readbytes(hi2c, 0x80 | LSM9DS0_REGISTER_OUT_X_L_G, buffer, 6);
  
  // Shift values to create properly formed integer (low byte first)
  gyroData->x = (buffer[1] << 8) | buffer[0];
  gyroData->y = (buffer[3] << 8) | buffer[2];
  gyroData->z = (buffer[5] << 8) | buffer[4];
}
