/**
  ******************************************************************************
  * @file       readme.txt 
  * @author     Fahad Mirza
  * @version    V1.0
  * @created    28-June-2016
  * @modified   04-July-2016
  * @brief      Read from Adafruit LSM9DS0 using I2C from STM32F407VG.
  ******************************************************************************

@ Code Description

This code describes how to perform I2C data transmission/reception between 
STM32F4 Discovery board and Adafruit LSM9DS0 in Polling mode.



@Hardware Connection
   _________________________                        _________________________
  |           ______________|                      |______________           |
  |          | I2C1         |                      |              |          |
  |          |              |                      |              |          |
  |          |      SCL(PB6)|______________________| SCL          |          |
  |          |              |                      |              |          |
  |          |              |                      |              |          |
  |          |              |                      |              |          |
  |          |      SDA(PB9)|______________________| SDA          |          |
  |          |              |                      |              |          |
  |          |______________|                      |______________|          |
  |      __                 |                      |                         |
  |     |__|                |                      |                         |
  |     USER             5v |______________________| Vin                     |
  |                      GND|______________________| GND                     |
  |_STM32F4 ________________|                      |_________________LSM9DS0_|
  
  
   _________________________                        _________________________
  |           ______________|                      |______________           |
  |          | I2C1         |                      |              |          |
  |          |              |                      |              |          |
  |          |      RxI(PA3)|______________________| TxO          |          |
  |          |              |                      |              |          |
  |          |              |                      |              |          |
  |          |              |                      |              |          |
  |          |      TxO(PA2)|______________________| RxI          |          |
  |          |              |                      |              |          |
  |          |______________|                      |______________|          |
  |      __                 |                      |                         |
  |     |__|                |                      |                         |
  |     USER                |                      |                         |
  |                      GND|______________________| GND                     |
  |_STM32F4 ________________|                      |______________FTDI Basic_|
  
  
  
@ How to use it ? 

In order to make the program work, you must do the following :
 - Open the project file in IAR Embedded Workbench (as of writing this I used Version 7.70). 
 - Download STM32CubeF4. BSP, HAL, CMSIS libraries were used from it (as of writing this I used Version 1.12).
 - Extract the Zip file. Make sure to update the Pre-Processor (Project->Options...->C/C++ Compiler) with the library path.
 - I2C and UART hardware connection is shown above.
 - For debug (UART2), I used FTDI basic from sparkfun. Baud 9600, 8N1. 
 - In main.c, define "Freefall Mode" (default) or "Stream Mode"
 - Rebuild all files and load your image into target memory.
 - Run the example
 - Orange LED (LED3) will blink until user press the blue button.
 - Once the blue button is pressed the code will start to execute.
 

@ Software

At the beginning of the main program the HAL_Init() function is called to reset 
all the peripherals, initialize the Flash interface and the systick.
Then the SystemClock_Config() function is used to configure the system
clock (SYSCLK) to run at 168 MHz.

The I2C peripheral configuration is ensured by the HAL_I2C_Init() function.
This later is calling the HAL_I2C_MspInit()function which core is implementing
the configuration of the needed I2C resources according to the used hardware (CLOCK & 
GPIO). That function was updated to change I2C configuration.

@note Do not use internal pull-up on PB6 and PB9. Adafruit LSM9DS0 has on board pull-up.

@note Care must be taken when using HAL_Delay(), this function provides accurate delay (in milliseconds)
      based on variable incremented in SysTick ISR.
      
@note The application needs to ensure that the SysTick time base is always set to 1 millisecond
      to have correct HAL operation.


@par Directory contents 

  - Src/main.c                 Main program
  - Src/system_stm32f4xx.c     STM32F4xx system clock configuration file
  - Src/stm32f4xx_it.c         Interrupt handlers 
  - Src/stm32f4xx_hal_msp.c    HAL MSP module
  - Src/lsm9dso.c              LSM9DS0 driver
  - Inc/main.h                 Main program header file  
  - Inc/stm32f4xx_hal_conf.h   HAL Configuration file
  - Inc/stm32f4xx_it.h         Interrupt handlers header file
  - Inc/lsm9dso.h              LSM9DS0 header file

        
@par Hardware and Software environment  

  - This example runs on STM32F407VG device.   
  - This example has been tested with STMicroelectronics STM32F4-Discovery 
    board.

 */
