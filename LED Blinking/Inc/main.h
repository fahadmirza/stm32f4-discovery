/**
  ******************************************************************************
  * @file    main.h 
  * @author  Fahad Mirza
  * @version V1.0
  * @date    27-June-2016
  * @brief   Header for main.c module
  ******************************************************************************
  * @attention
  *
  * This code is modified from the STM32F4Cube Template
  *
  ******************************************************************************
  */
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4_discovery.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif /* __MAIN_H */

