# README #

This repository contains codes for STM32F4-Discovery board.

### Important ###

* MCU: STM32F407VG
* IDE: IAR Embedded Workbench (Version 7.70).
* The code uses STM32Cube embedded software libraries(Version 1.12).
* If you download these codes make sure to update the 'Include Path' from "Project->Options...->C/C++ Compiler->Preprocessor" to include the BSP, CMSIS and HAL libraries.

* Go through readme.txt to know more of the correponding projects (e.g. Hardware connections etc.).